<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>2. 指定行数でテーブルを書いてみよう</title>
  </head>
  <body>
    <h1>2. 指定行数でテーブルを書いてみよう</h1>
    <p><!--段落-->
    <form method='post' action='loop01.php'>
        <input type="number" name="cols">行のテーブルを作成する
        <input type="submit">
    </form>
    <?php
        $loop = $_POST['cols'];
    ?>

    <table border="1" style="">
      <?php
        for($i=0; $i < $loop; $i++)
        {
            if($i % 2 == 0)
            {
                echo "<tr bgcolor = #ff0000>
                    <td>テスト</td>
                    </tr>";
            }
            else
            {
                echo "<tr bgcolor = #3fffff>
                    <td>テスト</td>
                    </tr>";
            }
        }
      ?>
    </table>

    </p>
  </body>
</html>
