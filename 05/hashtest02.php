<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>すべての要素を出力する（foreach())</title>
  </head>
  <body>
    <h1>すべての要素を出力する（foreach())</h1>
    <p>
        <table border="1" style="">
            <?php
            // 書き方その１
              $mydata = array
              (
                'fruit' => 'スイカ',
                'sport' => '野球',
                'town' => '横浜',
                'age' => 21,
                'food' => 'カレーライス'
               );

              foreach($mydata as $each)
              {
                echo "<td>". $each ."</td>"."<br/>";
              }

            ?>
            <!--
            <pre>
                <?php var_dump($mydata); ?>
            </pre>
        -->
        </table>
    </p>
  </body>
</html>
