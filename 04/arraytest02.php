<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>arraytest02.php</title>
  </head>
  <body>
    <h1>犬</h1>
    <p>
        <pre>
<!--
        <?php
        $sport = array("野球", "サッカー", "テニス", "帰宅部", "バトミントン", "ドッジボール");

            var_dump($sport); // 配列の中身がすべて表示される
        ?>
        <?php
        // 配列を定義
            $game = array("遊戯王", "原神", "APEX", "なし", "ARK", "valolant");
        // 配列の内容を表示

            for($i=0; $i < count($game) ; $i++)
            {
                echo $game[$i] . "<br/>";
            }
        ?>
        <?php
        // 配列を定義
            $inu = array("ミニチュアダックス", "ミニチュアピンシャー", "チワワ", "足長バチ", "柴犬", "ドーベルマン");
                $inu[3] ="プードル";
            foreach($inu as $key => $value)
            {
              echo $key . "番目の要素は" . $value . "です。<br/>";
            }
        ?>
        -->
        <?php
        // 配列を定義
            $inu = array("ミニチュアダックス", "ミニチュアピンシャー", "チワワ", "足長バチ", "柴犬", "ドーベルマン");
            // 配列の内容を表示
            var_dump($inu);

            // 検索する文字列
            $needle = "イカ";

            if(in_array($needle, $inu))
            {
                echo $needle . " がinuの要素の値に存在しています";
            }
            else
            {
                echo $needle . " がinuの要素の値に存在しません";
            }
        ?>
        </pre>
    </p>
  </body>
</html>
